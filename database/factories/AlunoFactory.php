<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\Aluno;

$factory->define(Aluno::class, function (Faker $faker) {
    return [
        'nome' => $faker->name,
        'endereco' => $faker->address,
        'cidade' => $faker->city,
    ];
});

$factory->define(Model::class, function (Faker $faker) {
    return [
        //
    ];
});
