<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use App\Models\Aluno;

class EscolaFakerTableSeeder extends Seeder
{
    public function run()
    {
        Aluno::truncate();
        factory(Aluno::class, 1000)->create();
    }
}
